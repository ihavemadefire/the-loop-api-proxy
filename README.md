# The Loop API Proxy

NGINX proxy app for The Loop

## Usage

### Environement Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests too (default: `9000`)

Author: [Jacob Ide](https://jacobmeide.com/)
